//scr_text("Text",speed,x,y,type);
txt = instance_create(argument2,argument3,obj_text);
with (txt)
{
    padding = 16;
    maxlength = 608;
    text = argument0;
    spd = argument1;
    font = fnt;
    //type 1 for player 2 for enemy
    type = argument4;
    
    text_length = string_length(text);
    font_size = font_get_size(font);
    
    draw_set_font(fnt);
    
    text_width = string_width_ext(text,font_size+(font_size/2), maxlength);
    text_height = string_height_ext(text,font_size+(font_size/2), maxlength);
    
    boxwidth = text_width + (padding*2);
    boxheight = text_height + (padding*2);
}
